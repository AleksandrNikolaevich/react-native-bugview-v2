import * as React from "react";
import { Event } from "./types";
declare const UIBugView: React.FC<{
    subscribeForceUpdate: (a: (timeline: Event) => void) => () => void;
    onClose: () => void;
    onSendLog: (path?: string) => void;
    onSendHistory: () => void;
}>;
export default UIBugView;
