import * as React from "react";
import { Event, EventType, Logger, TError } from "./types";
declare type Props = {
    onCrashReport: (uri: string) => Promise<void>;
    onSendLogs?: (uri: string) => Promise<void>;
    renderErrorScreen?: (props: {
        error: TError;
        savingReport: boolean;
        restartApp: () => void;
    }) => React.ReactNode;
};
declare type State = {
    error: TError | undefined;
    savingReport: boolean;
    showUI: boolean;
};
declare class BugView extends React.PureComponent<Props, State> {
    subscribtionUIForceUpdate: (a: Event) => void;
    logPath: string;
    lastSessions: string[];
    state: State;
    constructor(props: Props);
    componentDidMount(): void;
    componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void;
    checkExistCrash: () => Promise<void>;
    nativeErrorHandler: (error: string) => Promise<void>;
    jsErrorHandler: (error: Error, isFatal: boolean) => Promise<void>;
    createReport: (error: TError) => void;
    garbageCollection: () => Promise<void>;
    initLogger: () => void;
    initNetworkLogger: () => void;
    stringifyData: (type: EventType, data: any) => string;
    addEvent: (type: EventType) => (...data: any[]) => Promise<unknown>;
    sendLog: (path?: string) => Promise<void>;
    sendHistory: () => Promise<void>;
    sendCrash: () => Promise<void>;
    changeShowStateUI: (showUI: boolean) => void;
    render(): JSX.Element;
}
export default BugView;
export declare const logger: Logger & {
    testJSCrash: () => void;
};
