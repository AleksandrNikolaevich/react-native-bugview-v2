import { TDeviceInfo } from "./Device";
export declare type TError = Partial<Error> & {
    type: "js" | "native";
};
export declare enum EventType {
    debug = "debug",
    info = "info",
    warn = "warn",
    error = "error",
    request = "request",
    response = "response",
    navigate = "navigate",
    fatal = "fatal"
}
export declare type Event = {
    uuid: string;
    time: number;
    type: EventType;
    data: any[];
};
export declare type Log = {
    date: string;
    bugviewVersion: string;
    deviceInfo: TDeviceInfo;
    timeline: Event[];
    error?: TError;
};
export declare type LoggerAttribute = string | number | object | boolean;
export interface Logger {
    debug: (message: LoggerAttribute, ...data: LoggerAttribute[]) => void;
    info: (message: LoggerAttribute, ...data: LoggerAttribute[]) => void;
    warn: (message: LoggerAttribute, ...data: LoggerAttribute[]) => void;
    error: (message: LoggerAttribute, ...data: LoggerAttribute[]) => void;
    dump: () => void;
}
