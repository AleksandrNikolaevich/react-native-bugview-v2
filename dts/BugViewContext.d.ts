import * as React from "react";
export declare type TBugVIewContext = {
    navigationEvent: (screen: string, params?: any) => void;
    showUI: (state: boolean) => void;
    showingUI: boolean;
    bugviewVersion: string;
    sendLog: () => void;
    sendHistory: () => void;
};
declare const _default: React.Context<TBugVIewContext>;
export default _default;
