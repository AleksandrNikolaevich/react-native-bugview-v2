var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "./BugView", "./withBugView", "./useBugView", "./BugViewContext"], function (require, exports, BugView_1, withBugView_1, useBugView_1, BugViewContext_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.BugViewContext = exports.logger = exports.useBugView = exports.withBugView = void 0;
    BugView_1 = __importStar(BugView_1);
    withBugView_1 = __importDefault(withBugView_1);
    useBugView_1 = __importDefault(useBugView_1);
    BugViewContext_1 = __importDefault(BugViewContext_1);
    Object.defineProperty(exports, "logger", { enumerable: true, get: function () { return BugView_1.logger; } });
    exports.withBugView = withBugView_1.default;
    exports.useBugView = useBugView_1.default;
    exports.BugViewContext = BugViewContext_1.default;
    exports.default = BugView_1.default;
});
