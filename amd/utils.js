define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.safeStringify = void 0;
    function safeStringify(json) {
        try {
            var cache_1 = [];
            var result = JSON.stringify(json, function (key, value) {
                if (typeof value === "function") {
                    return "[Function]";
                }
                if (typeof value === 'object' && value !== null && Array.isArray(cache_1)) {
                    // Duplicate reference found, discard key
                    //@ts-ignore
                    if (cache_1.includes(value))
                        return "[Cylcular struct]";
                    // Store value in our collection
                    cache_1.push(value);
                }
                return value;
            });
            cache_1 = null;
            return result;
        }
        catch (e) {
            console.log("error@", e);
            return "bugview: Could not serialize data";
        }
    }
    exports.safeStringify = safeStringify;
});
