var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
define(["require", "exports", "react", "react-native", "./types", "react-native", "react", "react-native", "react-native", "react-native", "react-native-json-tree", "./utils"], function (require, exports, React, react_native_1, types_1, react_native_2, react_1, react_native_3, react_native_4, react_native_5, react_native_json_tree_1, utils_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    React = __importStar(React);
    react_native_json_tree_1 = __importDefault(react_native_json_tree_1);
    var screen = react_native_1.Dimensions.get("window");
    var UIBugView = function (_a) {
        var subscribeForceUpdate = _a.subscribeForceUpdate, onClose = _a.onClose, onSendLog = _a.onSendLog, onSendHistory = _a.onSendHistory;
        var _b = react_1.useState(), viewEvent = _b[0], setViewEvent = _b[1];
        var _c = react_1.useState('wait...'), eventText = _c[0], setTextEvent = _c[1];
        var _d = react_1.useState(false), minimize = _d[0], setMiniState = _d[1];
        var _e = react_1.useState([]), timeline = _e[0], setTimeline = _e[1];
        var _f = react_1.useState(), filterEvent = _f[0], setFilter = _f[1];
        var unsubscribe = React.useRef(function () { });
        React.useEffect(function () {
            unsubscribe.current = subscribeForceUpdate(function (event) {
                setTimeline(function (events) { return __spreadArrays(events, [event]); });
            });
            return function () {
                unsubscribe.current();
            };
        }, []);
        var formattedData = function (time) {
            var date = new Date(time);
            return date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "." + date.getMilliseconds();
        };
        var renderBody = function (type, data) {
            if (type === "request") {
                return data[0].method + " " + data[0].url;
            }
            if (type === "response") {
                return data[0].method + " " + data[0].url + "  (" + data[0].status + ")";
            }
            return "" + data.map(function (item) { return utils_1.safeStringify(item); }).join(" ");
        };
        var colorizeType = function (type) {
            var _a;
            var defaultColor = "white";
            var map = (_a = {},
                _a[types_1.EventType.debug] = defaultColor,
                _a[types_1.EventType.info] = "cyan",
                _a[types_1.EventType.warn] = "orange",
                _a[types_1.EventType.error] = "red",
                _a[types_1.EventType.navigate] = defaultColor,
                _a[types_1.EventType.response] = "rgb(10, 120, 255)",
                _a[types_1.EventType.request] = "rgb(255, 255, 150)",
                _a[types_1.EventType.fatal] = defaultColor,
                _a);
            return React.createElement(react_native_2.Text, { style: { color: map[type] || defaultColor } }, " [" + type + "]  ");
        };
        var getEventTypes = function () {
            var res = [];
            for (var eventName in types_1.EventType) {
                res.push(eventName);
            }
            return res;
        };
        var sendLogs = React.useCallback(function () {
            react_native_1.Alert.alert("Внимание", "Выберете действие", [
                { text: "Текущая сессиия", onPress: onSendLog },
                { text: "Прошлые сессиии", onPress: onSendHistory },
                { text: "Отмена", onPress: function () { }, style: "destructive" }
            ]);
        }, []);
        return (React.createElement(react_native_1.View, { style: [styles.root, !!viewEvent && { height: screen.height - 100 }, minimize && { height: "auto" },] },
            React.createElement(react_native_1.View, { style: styles.header },
                React.createElement(react_native_2.Text, { style: [{ flex: 1 }, styles.text] }, "BugView"),
                React.createElement(react_native_2.Text, { style: [styles.text, { marginHorizontal: 10 }], onPress: function () { return setTimeline([]); } }, "[clear]"),
                React.createElement(react_native_2.Text, { style: [styles.text, { marginHorizontal: 10 }], onPress: sendLogs }, "[send logs]"),
                React.createElement(react_native_2.Text, { style: [styles.text, { marginHorizontal: 10 }], onPress: function () { return setMiniState(function (s) { return !s; }); } }, "[minimize]"),
                React.createElement(react_native_2.Text, { style: [styles.text, { marginLeft: 10 }], onPress: onClose }, "\u0425")),
            viewEvent &&
                !minimize &&
                React.createElement(react_native_3.ScrollView, { contentContainerStyle: { paddingHorizontal: 20 } },
                    React.createElement(react_native_1.View, { style: [{ marginBottom: 10, flexDirection: "row" }] },
                        React.createElement(react_native_2.Text, { style: [styles.text, { flex: 1 }], onPress: function () { return setViewEvent(undefined); } }, "<-- Back"),
                        React.createElement(react_native_2.Text, { style: [styles.text], onPress: function () { return react_native_5.Clipboard.setString(eventText); } }, "[copy]")),
                    React.createElement(react_native_json_tree_1.default, { data: viewEvent })),
            !viewEvent &&
                !minimize &&
                React.createElement(react_native_3.ScrollView, { contentContainerStyle: { paddingHorizontal: 20, height: 40 }, showsHorizontalScrollIndicator: false, horizontal: true }, getEventTypes().map(function (eventName) { return (React.createElement(react_native_2.Text, { key: eventName, style: [
                        styles.text,
                        { marginRight: 20, marginVertical: 5 },
                        (!!filterEvent && filterEvent !== eventName) && { opacity: .5 }
                    ], onPress: function () { return setFilter(function (e) { if (e === eventName)
                        return undefined; return eventName; }); } }, colorizeType(eventName))); })),
            !viewEvent &&
                !minimize &&
                React.createElement(react_native_1.FlatList, { data: timeline.filter(function (e) { return e.type === filterEvent || !filterEvent; }).reverse(), keyExtractor: function (item) { return item.uuid; }, inverted: true, renderItem: function (_a) {
                        var item = _a.item;
                        var time = item.time, type = item.type, data = item.data;
                        return (React.createElement(react_native_4.TouchableOpacity, { onPress: function () {
                                setViewEvent(item);
                                setTextEvent('wait...');
                                setTimeout(function () {
                                    setTextEvent(utils_1.safeStringify(item));
                                }, 100);
                            }, style: { flexDirection: "row", paddingHorizontal: 20, marginVertical: 2 } },
                            React.createElement(react_native_2.Text, { style: [{ flex: 1 }, styles.text], numberOfLines: 2 }, "" + formattedData(time),
                                colorizeType(type),
                                renderBody(type, data)),
                            React.createElement(react_native_2.Text, { style: styles.text }, "-->")));
                    }, ItemSeparatorComponent: function () { return React.createElement(react_native_1.View, { style: styles.separator }); } })));
    };
    var styles = react_native_1.StyleSheet.create({
        header: {
            flexDirection: "row",
            paddingHorizontal: 20,
            paddingVertical: 5,
            backgroundColor: "rgba(0,0,0,.3)"
        },
        root: {
            position: "absolute",
            bottom: 0,
            left: 0,
            right: 0,
            height: 340,
            backgroundColor: "rgba(0,0,0,.7)"
        },
        text: {
            color: "white"
        },
        separator: {
            height: react_native_1.StyleSheet.hairlineWidth,
            width: "100%",
            backgroundColor: "white",
            opacity: .4
        }
    });
    exports.default = UIBugView;
});
