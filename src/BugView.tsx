import AsyncStorage from "@react-native-community/async-storage";
import moment from "moment";
import * as React from "react";
import { setJSExceptionHandler, setNativeExceptionHandler } from "react-native-exception-handler";
import fs from "react-native-fs";
import uuid from "react-native-uuid";
import BugViewContext from "./BugViewContext";
import Device from "./Device";
import NetworkLogger from "./NetworkLogger";
import { Event, EventType, Logger, TError } from "./types";
import UIBugView from "./UIBugView";
import { safeStringify } from "./utils";

type Props = {
    onCrashReport: (uri: string) => Promise<void>,
    onSendLogs?: (uri: string) => Promise<void>,
    renderErrorScreen?: (props: { error: TError, savingReport: boolean, restartApp: () => void }) => React.ReactNode,
}

type State = {
    error: TError | undefined,
    savingReport: boolean,
    showUI: boolean
}


const _settings = "bugview/settings"
const loggerPath = fs.DocumentDirectoryPath + `/logger/`;
const logsPath = loggerPath + `logs`;
const crashPath = loggerPath + `crash`;
const crashFilePath = crashPath + "/report.txt"
function format(date: Date, format: string = "DD.MM.YYYY") {
    return moment(date).format(format)
}

const bugviewVersion = require("../package.json").version;

const networkLogger = new NetworkLogger();



let _logger: Logger = {
    debug: () => { },
    info: () => { },
    warn: () => { },
    error: () => { },
    dump: () => { },
}

function generateLogPath() {
    return logsPath + '/' + uuid.v4() + '.txt';
}

class BugView extends React.PureComponent<Props, State>{
    subscribtionUIForceUpdate = (a: Event) => { }
    logPath = generateLogPath();
    lastSessions: string[] = []
    state: State = {
        error: undefined,
        savingReport: false,
        showUI: false
    }


    constructor(props: Props) {
        super(props);
        this.initLogger();
        fs.mkdir(crashPath).catch();
        setJSExceptionHandler(this.jsErrorHandler, false);
        setNativeExceptionHandler(this.nativeErrorHandler, false, true);
    }


    componentDidMount() {
        const { onCrashReport } = this.props;
        if (!onCrashReport) return;
        this.initNetworkLogger();
        logger.info("Bugview version", bugviewVersion);
        Device
            .getInfo()
            .then(this.addEvent(EventType.info));
        this.garbageCollection();

        this.checkExistCrash();

        AsyncStorage.getItem(_settings).then(val => {
            if (!val) return;
            const settings = JSON.parse(val);
            this.setState({ showUI: settings.showUI })
        })
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo) {
        this.createReport({
            type: "js",
            name: error.name,
            message: error.message,
            stack: error.stack,
        })
    }

    checkExistCrash = async () => {
        try {
            const file = await fs
                .stat(crashFilePath);
            if (!file) return;
            this.sendCrash();
        } catch (e) {

        }
    }

    nativeErrorHandler = async (error: string) => {
        this.createReport({
            type: "native",
            message: error
        })

    }

    jsErrorHandler = async (error: Error, isFatal: boolean) => {
        this.createReport({
            type: "js",
            name: error.name,
            message: error.message,
            stack: error.stack,
        })

    }


    createReport = (error: TError) => {
        this.addEvent(EventType.fatal)(error);
        this.setState({ error, savingReport: true });
        fs
            .copyFile(this.logPath, crashFilePath)
            .then(() => {
                this.logPath = crashFilePath;
                return this.sendCrash();
            })
            .then(() => {
                this.setState({ savingReport: false })
            })
            .catch((e) => {
                this.setState({ savingReport: false });
            })
    }


    garbageCollection = async () => {
        //храним только последние 5 сессий
        try {
            await fs.mkdir(logsPath);
        } catch (e) {

        }
        try {
            const items = await fs
                .readDir(logsPath)

            const files = items
                .filter((item) => item.isFile())
                .sort((a, b) => a.mtime!.getTime() - b.mtime!.getTime());
            if (files.length > 4) {
                fs
                    .unlink(files[0].path)
                    .catch();
                files.shift()
            }

            this.lastSessions = files.map(file => file.path);

        } catch (e) {

        }
    }

    initLogger = () => {
        _logger = {
            debug: (...argumentsList) => {
                if (!__DEV__) return;
                this.addEvent(EventType.debug)(...argumentsList);
                return console.log(...argumentsList);
            },
            info: (...argumentsList) => {
                this.addEvent(EventType.info)(...argumentsList);
                if (!__DEV__) return;
                return console.log(...argumentsList);
            },
            warn: (...argumentsList) => {
                this.addEvent(EventType.warn)(...argumentsList);
                if (!__DEV__) return;
                return console.warn(...argumentsList);
            },
            error: async (...argumentsList) => {
                try {
                    if (__DEV__) {
                        console.error(...argumentsList);
                    }
                    await this.addEvent(EventType.error)(...argumentsList);
                    if (!__DEV__) {
                        this.sendLog();
                        return;
                    }
                } catch (e) {

                }

            },
            dump: this.sendLog
        }
    }

    initNetworkLogger = () => {
        networkLogger.setCallback(this.addEvent(EventType.response));
        networkLogger.setStartRequestCallback(this.addEvent(EventType.request));
        networkLogger.enableXHRInterception();
    }



    stringifyData = (type: EventType, data: any) => {
        try {
            if (typeof data === "string") return data;
            if (type === EventType.response) return safeStringify({ ...data, response: '[Object]' });
            return safeStringify(data);
        } catch (e) {
            return `bugview: Could not serialize data`
        }

    }


    addEvent = (type: EventType) => (...data: any[]) => {
        return new Promise((resolve, reject) => {
            const output = `${format(new Date(), 'DD.MM.YYYY HH:mm:ss.SSS')} [${type}] ${data.map((arg) => this.stringifyData(type, arg)).join("\n")}\n\n`;

            fs.appendFile(this.logPath, output, 'utf8')
                .then(() => {
                    resolve()
                })
                .catch((err: any) => {
                    console.error(err);
                    reject()
                });

            this.subscribtionUIForceUpdate({
                uuid: uuid.v4(),
                time: Date.now(),
                type,
                data
            });
        })

    }

    sendLog = async (path: string = this.logPath) => {
        const { onSendLogs } = this.props;
        if (!onSendLogs) return;
        try {
            await onSendLogs(path);
        } catch (e) {

        }
    }

    sendHistory = async () => {
        this.lastSessions.forEach(filePath => {
            this.sendLog(filePath)
        });
    }

    sendCrash = async () => {
        const { onCrashReport } = this.props;
        if (!onCrashReport) return;
        try {
            await onCrashReport(crashFilePath);
            this.logPath = generateLogPath();
            await fs.unlink(crashFilePath);
        } catch (e) {

        }
    }

    // sendLog = async () => {
    //     const { onCrashReport } = this.props;
    //     if (!onCrashReport) return;

    //     try {
    //         await onCrashReport(logFile);
    //         fs.unlink(logFile)
    //     } catch (e) {

    //     }
    // }


    changeShowStateUI = (showUI: boolean) => {
        if (showUI) {
            AsyncStorage.setItem(_settings, JSON.stringify({ showUI: true }));
        } else {
            AsyncStorage.mergeItem(_settings, JSON.stringify({ showUI: false }));
        }

        this.setState({ showUI })
    }



    render() {
        const { renderErrorScreen } = this.props;
        const { error, savingReport, showUI } = this.state;


        const showErrorScreen = !!(error && renderErrorScreen);


        return <BugViewContext.Provider
            value={{
                navigationEvent: (screen, params) => {
                    this.addEvent(EventType.navigate)(screen, params)
                },
                showingUI: showUI,
                showUI: this.changeShowStateUI,
                bugviewVersion,
                sendLog: this.sendLog,
                sendHistory: this.sendHistory,
            }}
        >
            {
                !!showErrorScreen &&
                renderErrorScreen!({
                    error: error!,
                    savingReport,
                    restartApp: () => {
                        this.sendCrash();
                        this.setState({ error: undefined });
                    }
                })
            }
            {!showErrorScreen && this.props.children}
            {
                !!showUI &&
                <UIBugView
                    onSendLog={this.sendLog}
                    onSendHistory={this.sendHistory}
                    onClose={() => this.changeShowStateUI(false)}
                    subscribeForceUpdate={(f) => {
                        this.subscribtionUIForceUpdate = f;
                        return () => {
                            this.subscribtionUIForceUpdate = () => { }
                        }
                    }}
                />
            }
        </BugViewContext.Provider>

    }
}

export default BugView;

export const logger: Logger & { testJSCrash: () => void } = {
    debug: (...args) => _logger.debug(...args),
    info: (...args) => _logger.info(...args),
    warn: (...args) => _logger.warn(...args),
    error: (...args) => _logger.error(...args),
    dump: () => _logger.dump(),
    //@ts-ignore
    testJSCrash: () => a.js()
}