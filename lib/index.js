"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BugViewContext = exports.logger = exports.useBugView = exports.withBugView = void 0;
var BugView_1 = __importStar(require("./BugView"));
Object.defineProperty(exports, "logger", { enumerable: true, get: function () { return BugView_1.logger; } });
var withBugView_1 = __importDefault(require("./withBugView"));
exports.withBugView = withBugView_1.default;
var useBugView_1 = __importDefault(require("./useBugView"));
exports.useBugView = useBugView_1.default;
var BugViewContext_1 = __importDefault(require("./BugViewContext"));
exports.BugViewContext = BugViewContext_1.default;
exports.default = BugView_1.default;
