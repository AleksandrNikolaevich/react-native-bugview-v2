"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
var async_storage_1 = __importDefault(require("@react-native-community/async-storage"));
var moment_1 = __importDefault(require("moment"));
var React = __importStar(require("react"));
var react_native_exception_handler_1 = require("react-native-exception-handler");
var react_native_fs_1 = __importDefault(require("react-native-fs"));
var react_native_uuid_1 = __importDefault(require("react-native-uuid"));
var BugViewContext_1 = __importDefault(require("./BugViewContext"));
var Device_1 = __importDefault(require("./Device"));
var NetworkLogger_1 = __importDefault(require("./NetworkLogger"));
var types_1 = require("./types");
var UIBugView_1 = __importDefault(require("./UIBugView"));
var utils_1 = require("./utils");
var _settings = "bugview/settings";
var loggerPath = react_native_fs_1.default.DocumentDirectoryPath + "/logger/";
var logsPath = loggerPath + "logs";
var crashPath = loggerPath + "crash";
var crashFilePath = crashPath + "/report.txt";
function format(date, format) {
    if (format === void 0) { format = "DD.MM.YYYY"; }
    return moment_1.default(date).format(format);
}
var bugviewVersion = require("../package.json").version;
var networkLogger = new NetworkLogger_1.default();
var _logger = {
    debug: function () { },
    info: function () { },
    warn: function () { },
    error: function () { },
    dump: function () { },
};
function generateLogPath() {
    return logsPath + '/' + react_native_uuid_1.default.v4() + '.txt';
}
var BugView = /** @class */ (function (_super) {
    __extends(BugView, _super);
    function BugView(props) {
        var _this = _super.call(this, props) || this;
        _this.subscribtionUIForceUpdate = function (a) { };
        _this.logPath = generateLogPath();
        _this.lastSessions = [];
        _this.state = {
            error: undefined,
            savingReport: false,
            showUI: false
        };
        _this.checkExistCrash = function () { return __awaiter(_this, void 0, void 0, function () {
            var file, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, react_native_fs_1.default
                                .stat(crashFilePath)];
                    case 1:
                        file = _a.sent();
                        if (!file)
                            return [2 /*return*/];
                        this.sendCrash();
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        }); };
        _this.nativeErrorHandler = function (error) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.createReport({
                    type: "native",
                    message: error
                });
                return [2 /*return*/];
            });
        }); };
        _this.jsErrorHandler = function (error, isFatal) { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                this.createReport({
                    type: "js",
                    name: error.name,
                    message: error.message,
                    stack: error.stack,
                });
                return [2 /*return*/];
            });
        }); };
        _this.createReport = function (error) {
            _this.addEvent(types_1.EventType.fatal)(error);
            _this.setState({ error: error, savingReport: true });
            react_native_fs_1.default
                .copyFile(_this.logPath, crashFilePath)
                .then(function () {
                _this.logPath = crashFilePath;
                return _this.sendCrash();
            })
                .then(function () {
                _this.setState({ savingReport: false });
            })
                .catch(function (e) {
                _this.setState({ savingReport: false });
            });
        };
        _this.garbageCollection = function () { return __awaiter(_this, void 0, void 0, function () {
            var e_2, items, files, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, react_native_fs_1.default.mkdir(logsPath)];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        return [3 /*break*/, 3];
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, react_native_fs_1.default
                                .readDir(logsPath)];
                    case 4:
                        items = _a.sent();
                        files = items
                            .filter(function (item) { return item.isFile(); })
                            .sort(function (a, b) { return a.mtime.getTime() - b.mtime.getTime(); });
                        if (files.length > 4) {
                            react_native_fs_1.default
                                .unlink(files[0].path)
                                .catch();
                            files.shift();
                        }
                        this.lastSessions = files.map(function (file) { return file.path; });
                        return [3 /*break*/, 6];
                    case 5:
                        e_3 = _a.sent();
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        }); };
        _this.initLogger = function () {
            _logger = {
                debug: function () {
                    var argumentsList = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        argumentsList[_i] = arguments[_i];
                    }
                    if (!__DEV__)
                        return;
                    _this.addEvent(types_1.EventType.debug).apply(void 0, argumentsList);
                    return console.log.apply(console, argumentsList);
                },
                info: function () {
                    var argumentsList = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        argumentsList[_i] = arguments[_i];
                    }
                    _this.addEvent(types_1.EventType.info).apply(void 0, argumentsList);
                    if (!__DEV__)
                        return;
                    return console.log.apply(console, argumentsList);
                },
                warn: function () {
                    var argumentsList = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        argumentsList[_i] = arguments[_i];
                    }
                    _this.addEvent(types_1.EventType.warn).apply(void 0, argumentsList);
                    if (!__DEV__)
                        return;
                    return console.warn.apply(console, argumentsList);
                },
                error: function () {
                    var argumentsList = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        argumentsList[_i] = arguments[_i];
                    }
                    return __awaiter(_this, void 0, void 0, function () {
                        var e_4;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    _a.trys.push([0, 2, , 3]);
                                    if (__DEV__) {
                                        console.error.apply(console, argumentsList);
                                    }
                                    return [4 /*yield*/, this.addEvent(types_1.EventType.error).apply(void 0, argumentsList)];
                                case 1:
                                    _a.sent();
                                    if (!__DEV__) {
                                        this.sendLog();
                                        return [2 /*return*/];
                                    }
                                    return [3 /*break*/, 3];
                                case 2:
                                    e_4 = _a.sent();
                                    return [3 /*break*/, 3];
                                case 3: return [2 /*return*/];
                            }
                        });
                    });
                },
                dump: _this.sendLog
            };
        };
        _this.initNetworkLogger = function () {
            networkLogger.setCallback(_this.addEvent(types_1.EventType.response));
            networkLogger.setStartRequestCallback(_this.addEvent(types_1.EventType.request));
            networkLogger.enableXHRInterception();
        };
        _this.stringifyData = function (type, data) {
            try {
                if (typeof data === "string")
                    return data;
                if (type === types_1.EventType.response)
                    return utils_1.safeStringify(__assign(__assign({}, data), { response: '[Object]' }));
                return utils_1.safeStringify(data);
            }
            catch (e) {
                return "bugview: Could not serialize data";
            }
        };
        _this.addEvent = function (type) { return function () {
            var data = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                data[_i] = arguments[_i];
            }
            return new Promise(function (resolve, reject) {
                var output = format(new Date(), 'DD.MM.YYYY HH:mm:ss.SSS') + " [" + type + "] " + data.map(function (arg) { return _this.stringifyData(type, arg); }).join("\n") + "\n\n";
                react_native_fs_1.default.appendFile(_this.logPath, output, 'utf8')
                    .then(function () {
                    resolve();
                })
                    .catch(function (err) {
                    console.error(err);
                    reject();
                });
                _this.subscribtionUIForceUpdate({
                    uuid: react_native_uuid_1.default.v4(),
                    time: Date.now(),
                    type: type,
                    data: data
                });
            });
        }; };
        _this.sendLog = function (path) {
            if (path === void 0) { path = _this.logPath; }
            return __awaiter(_this, void 0, void 0, function () {
                var onSendLogs, e_5;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            onSendLogs = this.props.onSendLogs;
                            if (!onSendLogs)
                                return [2 /*return*/];
                            _a.label = 1;
                        case 1:
                            _a.trys.push([1, 3, , 4]);
                            return [4 /*yield*/, onSendLogs(path)];
                        case 2:
                            _a.sent();
                            return [3 /*break*/, 4];
                        case 3:
                            e_5 = _a.sent();
                            return [3 /*break*/, 4];
                        case 4: return [2 /*return*/];
                    }
                });
            });
        };
        _this.sendHistory = function () { return __awaiter(_this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                this.lastSessions.forEach(function (filePath) {
                    _this.sendLog(filePath);
                });
                return [2 /*return*/];
            });
        }); };
        _this.sendCrash = function () { return __awaiter(_this, void 0, void 0, function () {
            var onCrashReport, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        onCrashReport = this.props.onCrashReport;
                        if (!onCrashReport)
                            return [2 /*return*/];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 4, , 5]);
                        return [4 /*yield*/, onCrashReport(crashFilePath)];
                    case 2:
                        _a.sent();
                        this.logPath = generateLogPath();
                        return [4 /*yield*/, react_native_fs_1.default.unlink(crashFilePath)];
                    case 3:
                        _a.sent();
                        return [3 /*break*/, 5];
                    case 4:
                        e_6 = _a.sent();
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        }); };
        // sendLog = async () => {
        //     const { onCrashReport } = this.props;
        //     if (!onCrashReport) return;
        //     try {
        //         await onCrashReport(logFile);
        //         fs.unlink(logFile)
        //     } catch (e) {
        //     }
        // }
        _this.changeShowStateUI = function (showUI) {
            if (showUI) {
                async_storage_1.default.setItem(_settings, JSON.stringify({ showUI: true }));
            }
            else {
                async_storage_1.default.mergeItem(_settings, JSON.stringify({ showUI: false }));
            }
            _this.setState({ showUI: showUI });
        };
        _this.initLogger();
        react_native_fs_1.default.mkdir(crashPath).catch();
        react_native_exception_handler_1.setJSExceptionHandler(_this.jsErrorHandler, false);
        react_native_exception_handler_1.setNativeExceptionHandler(_this.nativeErrorHandler, false, true);
        return _this;
    }
    BugView.prototype.componentDidMount = function () {
        var _this = this;
        var onCrashReport = this.props.onCrashReport;
        if (!onCrashReport)
            return;
        this.initNetworkLogger();
        exports.logger.info("Bugview version", bugviewVersion);
        Device_1.default
            .getInfo()
            .then(this.addEvent(types_1.EventType.info));
        this.garbageCollection();
        this.checkExistCrash();
        async_storage_1.default.getItem(_settings).then(function (val) {
            if (!val)
                return;
            var settings = JSON.parse(val);
            _this.setState({ showUI: settings.showUI });
        });
    };
    BugView.prototype.componentDidCatch = function (error, errorInfo) {
        this.createReport({
            type: "js",
            name: error.name,
            message: error.message,
            stack: error.stack,
        });
    };
    BugView.prototype.render = function () {
        var _this = this;
        var renderErrorScreen = this.props.renderErrorScreen;
        var _a = this.state, error = _a.error, savingReport = _a.savingReport, showUI = _a.showUI;
        var showErrorScreen = !!(error && renderErrorScreen);
        return React.createElement(BugViewContext_1.default.Provider, { value: {
                navigationEvent: function (screen, params) {
                    _this.addEvent(types_1.EventType.navigate)(screen, params);
                },
                showingUI: showUI,
                showUI: this.changeShowStateUI,
                bugviewVersion: bugviewVersion,
                sendLog: this.sendLog,
                sendHistory: this.sendHistory,
            } },
            !!showErrorScreen &&
                renderErrorScreen({
                    error: error,
                    savingReport: savingReport,
                    restartApp: function () {
                        _this.sendCrash();
                        _this.setState({ error: undefined });
                    }
                }),
            !showErrorScreen && this.props.children,
            !!showUI &&
                React.createElement(UIBugView_1.default, { onSendLog: this.sendLog, onSendHistory: this.sendHistory, onClose: function () { return _this.changeShowStateUI(false); }, subscribeForceUpdate: function (f) {
                        _this.subscribtionUIForceUpdate = f;
                        return function () {
                            _this.subscribtionUIForceUpdate = function () { };
                        };
                    } }));
    };
    return BugView;
}(React.PureComponent));
exports.default = BugView;
exports.logger = {
    debug: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return _logger.debug.apply(_logger, args);
    },
    info: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return _logger.info.apply(_logger, args);
    },
    warn: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return _logger.warn.apply(_logger, args);
    },
    error: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return _logger.error.apply(_logger, args);
    },
    dump: function () { return _logger.dump(); },
    //@ts-ignore
    testJSCrash: function () { return a.js(); }
};
