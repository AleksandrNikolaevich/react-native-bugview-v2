"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventType = void 0;
var EventType;
(function (EventType) {
    EventType["debug"] = "debug";
    EventType["info"] = "info";
    EventType["warn"] = "warn";
    EventType["error"] = "error";
    EventType["request"] = "request";
    EventType["response"] = "response";
    EventType["navigate"] = "navigate";
    EventType["fatal"] = "fatal";
})(EventType = exports.EventType || (exports.EventType = {}));
